import {Clothes, ClothingPart, Clothing, processClothingPartLocation} from "./clothing";
import {Layer} from "../util/canvas";
import {locateRelativeToDrawpoint, Location} from "..";
import {
    drawPoints,
    rad,
    point,
    norm,
    diff,
	getPointOnCurve,
	drawCircle,
	breakPoint,
	
	extractPoint,
    splitCurve,
    adjust,
    reflect,
	
} from "drawpoint";

import {NeckAccessory} from "./neck_accessory";

import {
	//getLacingPoints,
	findBetween,
	drawStar,
	copyCurve
} from "../util/auxiliary";

function calcNeck(ex){
	let temp = splitCurve(this.neckCoverage, ex.neck.cusp, ex.collarbone);
	let top  = temp.left.p2;
		top.y -= 0.5 * this.thickness;
		temp = splitCurve(this.cleavageCoverage, ex.neck.cusp, ex.groin);
	let bot = temp.left.p2;
		bot.x = 0;
		
	top.cp1 = {
		x: top.x * 0.5 + bot.x * 0.5,
		y: bot.y 
	};
	top.cp1.x -= 1;
	top.cp1.x += this.curveX;
	top.cp1.y += this.curveY;
		
	return {
		top,
		bot
	};
}

export class BiChainPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "+neck",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, {
			//chain: false,
			//neckCoverage: 0.14,
			//cleavageCoverage: 0.09,
			curveX: 0,
			curveY: 0,
			thickness: 1,
			
			//beadsSize: 4,
			//spaceSize: 4,
			//beadThickness: 1,
			//highlight: "red"
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);			
		const {top,bot} = calcNeck.call(this, ex);
		
		ctx.beginPath();
		ctx.lineWidth = this.thickness;
		drawPoints(ctx,
			bot,
			top
		);
		ctx.stroke();		
		
		ctx.beginPath();
		if(this.chain){
			ctx.setLineDash([this.beadsSize, this.spaceSize])
		}
		ctx.strokeStyle = this.highlight; //ctx.strokeStyle = this.fillStyle would be better
		ctx.lineWidth = this.beadThickness;
		drawPoints(ctx,
			bot,
			top
		);
		ctx.stroke();	
    }
}


export class TearPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "+neck",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, {
			width: 3.5,
			length: 0.5,
			size: 1,
        }, ...data);
    }  

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);			
		if(this.highlight){
			ctx.fillStyle = this.highlight;
		}
		
		const {top,bot} = calcNeck.call(this, ex);
		
		let bottom = adjust(bot,0,-6*this.size);
		bottom.cp1 = adjust(bottom,this.width*this.size,this.length*this.size);
		
		ctx.beginPath();
		drawPoints(ctx,
			bot,
			bottom
		);
		ctx.fill();		
    }
}


export class DoubleNecklacePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "+neck",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, {
			neckCoverage: 0.14,
			cleavageCoverage: 0.13,
			cleavageCoverageTop: 0.05,
			curveX: 0,
			curveY: 0,
			thickness: 0.5,
			
			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);			
		const {top,bot} = calcNeck.call(this, ex);
		
		if(this.chain){
			ctx.setLineDash([3, 3])
		}
		ctx.lineWidth = this.thickness;
		
		ctx.beginPath();
		drawPoints(ctx,
			bot,
			top
		);
		ctx.stroke();	

		if(this.cleavageCoverageTop>this.cleavageCoverage-0.03){
			this.cleavageCoverageTop = this.cleavageCoverage - 0.03;
		}
		let temp = splitCurve(this.cleavageCoverageTop, ex.neck.cusp, ex.groin);
		
		let bot2 = temp.left.p2;
		bot2.x = 0;
		bot2.cp1 = {
			x: bot2.x * 0.5 + top.x * 0.5,
			y: bot2.y 
		};
		
		ctx.beginPath();
		drawPoints(ctx,
			top,
			bot2,
		);
		ctx.stroke();
    }
}


export class MultiNecklacePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "+neck",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, {
			cleavageCoverage: 0.13,
			neckCoverage: 0.05,
			curveX: 0,
			curveY: 0,
			thickness: 0.5,
			
			size: 1.2,
			multiple: 2,
			distance: 5
        }, ...data);
    }


    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);			
		const {top,bot} = calcNeck.call(this, ex);
		
		if(this.chain){
			ctx.setLineDash([3, 3])
		}
		ctx.lineWidth = this.thickness;
		ctx.beginPath();
		drawPoints(ctx,
			bot,
			top,
		);
		ctx.stroke();
		
		let space = -this.distance;
		
		for(let i = 0; i < this.multiple; i++){
			let low = adjust(bot,0,space)
			let up = adjust(top,0.02,0)
			//copyCurve(bot,top,low,up);
			up.cp1 = {
				x: up.x * 0.5 + low.x * 0.5+ this.curveX + 4*i,
				y: low.y + this.curveY + i
			};
			
			ctx.beginPath();
			drawPoints(ctx,
				low,
				up
			);
			ctx.stroke();
			space -= this.distance;
		}
		
	}
}


export class ChainPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "+neck",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, {
			chain: false,
			neckCoverage: 0.14,
			cleavageCoverage: 0.09,
			curveX: 0,
			curveY: 0,
			thickness: 0.5,
			dash: 2,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);			
		const {top,bot} = calcNeck.call(this, ex);
		
		ctx.beginPath();
		if(this.chain){
			ctx.setLineDash([this.dash, this.dash])
		}
		ctx.lineWidth = this.thickness;
		drawPoints(ctx,
			bot,
			top
		);
		ctx.stroke();		
    }
}
 
 
export class StarPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "+neck",
        //    reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, {
			starThickness: 0.6,
			radius: 4,
			spikes: 5,
			upwards: true,
			styleOuter: 1, //1 = circle around; 0 = nothing; -1 = polygon around 
			styleInner: -1, //1 = circle inside; 0 = nothing; -1 = polygon inside 
        }, ...data);
    }  

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);			
		const {top,bot} = calcNeck.call(this, ex);
			
		//drawStar({x,y},spikes,outerRadius,innerRadius,up = true,outer = 0, inner = 0){
		let points = drawStar({x: 0, y: bot.y - this.radius}, this.spikes, this.radius, 0.33 * this.radius, this.upwards, this.styleOuter, this.styleInner )
		
		ctx.beginPath();
		ctx.lineWidth = this.starThickness;
		ctx.strokeStyle = ctx.fillStyle;
		drawPoints(ctx,
			...points,
		);
		ctx.stroke();		
    }
}


export class TNecklacePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "+neck",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, {
			chain: false,
			neckCoverage: 0.14,
			cleavageCoverage: 0.09,
			curveX: 0,
			curveY: 0,
			thickness: 0.5,
			
			size: 1.2
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);			
		const {top,bot} = calcNeck.call(this, ex);
		const bottom = adjust(bot,0,-12) 
		
		ctx.lineWidth = this.thickness;
		ctx.beginPath();
		drawPoints(ctx,
			bottom,
			bot,
			top,
		);
		ctx.stroke();	

		ball( getPointOnCurve(0.4,bot,top), this.size );
		ball( getPointOnCurve(0.8,bot,top), this.size );
		ball( findBetween(bot,bottom,0.3), this.size );
		ball( findBetween(bot,bottom,0.7), this.size );
		function ball(center,size){
			let points = drawCircle(center, size); 
			ctx.beginPath();
			drawPoints(ctx,
				...points
			);
			ctx.fill();	
		};
				
    }
}


/**
 * Base Clothing classes
 */
export class Necklace extends NeckAccessory {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.OUTER,
            stroke       : "#5c5c5c",
            fill         : "#5c5c5c",
        }, ...data);

    }
}

export class BiChain extends Necklace {
    constructor(...data) {
        super({
			cleavageCoverage: 0.1,
			chain: true,
			curveX: 2,
			cleavageCoverage: 0.1,
			neckCoverage: 0.07,
			
			
			beadsSize: 4,
			spaceSize: 8,
			beadThickness: 1,
			thickness: 1,
			
			highlight: "red"
			
        }, ...data);
    }

	stroke() {
        return "blue";
    }
	
    fill() {
        return "red";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: BiChainPart,
            }
        ];
    }
}

export class DoubleNecklace extends Necklace {
    constructor(...data) {
        super({
			cleavageCoverage: 0.13,
			cleavageCoverageTop: 0.05,
			chain: true,
			neckCoverage: 0.05,
			thickness: 0.7,
			highlight: "",
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: DoubleNecklacePart,
            },{
                side: null,
                Part: TearPart,
            }
        ];
    }
}

export class MultiNecklace extends Necklace {
    constructor(...data) {
        super({
			//curveX: -1,
			cleavageCoverage: 0.13,
			neckCoverage: 0.05,
			chain: true,
			curveX: 2,
			
			thickness: 0.7,
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: MultiNecklacePart,
            }
        ];
    }
}

export class PearlNecklace extends Necklace {
    constructor(...data) {
        super({
			//curveX: -1,
			cleavageCoverage: 0.1,
			chain: true,
			curveX: 2,
			cleavageCoverage: 0.1,
			neckCoverage: 0.07,
						
			beadsSize: 0.3,
			beadThickness: 2,
			spaceSize: 8.8,
			thickness: 0.2,
			highlight: "white",
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: BiChainPart,
            }
        ];
    }
}

export class SimpleChain extends Necklace {
    constructor(...data) {
        super({
			//curveX: -1,
			cleavageCoverage: 0.13,
			chain: true,
			curveX: 2,
			cleavageCoverage: 0.11,
			neckCoverage: 0.05,
			thickness: 0.6,	
			dash: 2.5
        }, ...data);
    }

	get partPrototypes() {
        return [
            {
                side: null,
                Part: ChainPart,
            }
        ];
    }
}

export class StarNecklace extends Necklace {
    constructor(...data) {
        super({
			curveX: -1,
			cleavageCoverage: 0.13,
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: ChainPart,
            },{
                side: null,
                Part: StarPart,
            }
        ];
    }
}

export class TNecklace extends Necklace {
    constructor(...data) {
        super({
			//curveX: -1,
			cleavageCoverage: 0.13,
			chain: true,
			curveX: 2,
			cleavageCoverage: 0.11,
			neckCoverage: 0.05,
			thickness: 0.7,
        }, ...data);
    }

	get partPrototypes() {
        return [
            {
                side: null,
                Part: TNecklacePart,
            }
        ];
    }
}

export class TearNecklace extends Necklace {
    constructor(...data) {
        super({
			cleavageCoverage: 0.13,
			chain: true,
			curveX: 0,
			cleavageCoverage: 0.11,
			neckCoverage: 0.05,
			thickness: 0.7,
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: ChainPart,
            },{
                side: null,
                Part: TearPart,
            }
        ];
    }
}

export class ThickChain extends Necklace {
    constructor(...data) {
        super({
			cleavageCoverage: 0.13,
			chain: true,
			curveX: 2,
			cleavageCoverage: 0.16,
			neckCoverage: 0.1,
			thickness: 1.5,
			dash: 5,
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: ChainPart,
            }
        ];
    }
}
import {Clothes, ClothingPart, Clothing, processClothingPartLocation} from "./clothing";
import {Layer} from "../util/canvas";
import {locateRelativeToDrawpoint, Location} from "..";
import {
    drawPoints,
    splitCurve,
	rad,
	drawCircle,
	breakPoint,
	extractPoint,
	adjust,
	reflect,
} from "drawpoint";

import {Jewelry} from "./jewelry";

import {
	findBetween,
	polar2cartesian,
	//cartesian2polar,
	copyCurve,
} from "../util/auxiliary";


class Bodychain1Part extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "+torso",
            reflect            : true,
        }, {			
			cleavageCoverage: 0.2,
			cleavageCoverageBot: 0.6,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
			Clothes.simpleStrokeFill(ctx, ex, this);		
		 
			//neck
			let points;
			let temp;
			//ctx.lineWidth = 0.7;
			const ringRadius = 3;
			
			//top ring 
			temp = splitCurve(this.cleavageCoverage, ex.neck.cusp, ex.groin);
			const pectoral = {x:0,y:temp.left.p2.y};
			points = drawCircle(pectoral, ringRadius);
			ctx.beginPath();
			drawPoints(ctx,...points);
			ctx.stroke();
			
			//bot ring 
			temp = splitCurve(this.cleavageCoverageBot, ex.neck.cusp, ex.groin);
			const under = {x:0,y:temp.left.p2.y};
			points = drawCircle(under, ringRadius);
			ctx.beginPath();
			drawPoints(ctx,...points);
			ctx.stroke();
			
			ctx.setLineDash([3, 3])
			
			//neck part			
			temp = splitCurve(0.1,ex.neck.cusp,ex.collarbone);
			const neck = temp.left.p2;
			const ringTop = polar2cartesian(ringRadius, rad(50), pectoral);
			ctx.beginPath();
			drawPoints(ctx,neck,ringTop);
			ctx.stroke();	
			
			//ring connection
			ctx.beginPath();
			drawPoints(ctx,
				{x:0,y:pectoral.y-ringRadius},
				{x:0,y:under.y+ringRadius}
			);
			ctx.stroke();
			
			//bottom
			const upRingBot = polar2cartesian(ringRadius, rad(0), under);
			temp = splitCurve(0.2,ex.waist,ex.hip);
			let upWaist = extractPoint(temp.left.p2);
			upWaist.cp1 = {
				x:ex.chest.nipples.x,
				y:upWaist.y
			};
			
			const lowRingBot = polar2cartesian(ringRadius, rad(-45), under);
			temp = splitCurve(0.8,ex.waist,ex.hip);
			let lowWaist = extractPoint(temp.left.p2);
			lowWaist.cp1 = {
				x:ex.chest.nipples.x,
				y:lowWaist.y
			};
			
			ctx.beginPath();
			drawPoints(ctx,upRingBot,upWaist,breakPoint,lowRingBot,lowWaist);
			ctx.stroke();
			
    }
}

 
class Bodychain2Part extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "+torso",
            reflect            : true,
        }, {			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
			Clothes.simpleStrokeFill(ctx, ex, this);		
			let points;
			let temp;
			
			//ctx.lineWidth = 0.7;
			const ringRadius = 3;
						
			//top ring 
			const pectoral = {x:0,y:ex.armpit.y+9};
			points = drawCircle(pectoral, ringRadius);
			ctx.beginPath();
			drawPoints(ctx,...points);
			ctx.stroke();
			
			//bot ring 
			const under = {x:0,y:ex.waist.y+12};

			ctx.setLineDash([3, 3])
			
			//neck part			
			temp = splitCurve(0.1,ex.neck.cusp,ex.collarbone);
			const neck = temp.left.p2;
			const ringTop = polar2cartesian(ringRadius, rad(50), pectoral);
			ctx.beginPath();
			drawPoints(ctx,neck,ringTop);
			ctx.stroke();	
			
			//bottom
			let waist = extractPoint(ex.hip);	
			waist.cp1 = {
				x:ex.armpit.x-4,
				y:waist.y
			};
			
			ctx.beginPath();
			drawPoints(ctx,
				{x:0,y:pectoral.y-ringRadius},
				under,
				waist
			);
			ctx.stroke();
			
			//connection top
			temp = da.splitCurve(0.1,under,waist);
			let upMid = temp.left.p2;
			let upBot = {x:0,y:upMid.y-4}
			upBot.cp1 = {
				x: upBot.x * 0.5 + upMid.x * 0.5,
				y: upBot.y
			};
					
			ctx.beginPath();
			da.drawPoints(ctx,upMid,upBot);
			ctx.stroke();
				
			//connection bot
			temp = da.splitCurve(0.2,under,waist);
			let lowMid = temp.left.p2;
			let lowBot = {x:0,y:lowMid.y-4};
			copyCurve(upMid,upBot,lowMid,lowBot)
					
			ctx.beginPath();
			da.drawPoints(ctx,lowMid,lowBot);
			ctx.stroke();	
    }
}

class Bodychain3Part extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "+torso",
            reflect            : true,
        }, {			
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
			Clothes.simpleStrokeFill(ctx, ex, this);		
		 
			let temp;
			//ctx.lineWidth = 0.7;
			
			const pectoral = {x:0,y:ex.armpit.y+10};
			const under = {x:0,y:ex.waist.y+8};
			
			//neck part			
			temp = splitCurve(0.3,ex.neck.cusp,ex.collarbone);
			const neck = temp.left.p2;
						
			//bottom
			temp = splitCurve(0.2,ex.waist,ex.hip);
			let waist = extractPoint(temp.left.p2);
			waist.cp1 = {
				x:ex.chest.nipples.x,
				y:waist.y
			};
			
			let bottom = adjust(under,0,-6);
			temp = splitCurve(0.4,ex.waist,ex.hip);
			let waist2 = extractPoint(temp.left.p2);
			copyCurve(under,waist,bottom,waist2);
			
			ctx.setLineDash([3, 3])
			ctx.beginPath();
			drawPoints(ctx,neck,pectoral,under,waist,breakPoint,under,bottom,waist2);
			ctx.stroke();
			
    }
}


/**
 * Base Clothing classes
 */
export class Bodychain extends Jewelry {
    constructor(...data) {
        super({
			
            clothingLayer: Clothes.Layer.BASE,
            stroke       : "#9c8530",
            fill         : "red",
			thickness	 : 0.7,
        }, ...data);

    }
}
/**/



export class Bodychain1 extends Bodychain {
    constructor(...data) {
        super({
			 
        }, ...data);
    }

    get partPrototypes() {
        return [
           {
                side: null,
                Part: Bodychain1Part,
            }
        ];
    }
}

export class Bodychain2 extends Bodychain {
    constructor(...data) {
        super({
			 
        }, ...data);
    }

    get partPrototypes() {
        return [
           {
                side: null,
                Part: Bodychain2Part,
            }
        ];
    }
}

export class Bodychain3 extends Bodychain {
    constructor(...data) {
        super({
			 
        }, ...data);
    }

    get partPrototypes() {
        return [
           {
                side: null,
                Part: Bodychain3Part,
            }
        ];
    }
}

/*
export class Bodychain4 extends Bodychain {
    constructor(...data) {
        super({
			 
        }, ...data);
    }

    get partPrototypes() {
        return [
           {
                side: null,
                Part: Bodychain1Part,
            }
        ];
    }
}
*/
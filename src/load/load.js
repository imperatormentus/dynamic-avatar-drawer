import loadPatterns from "./patterns";
import {loadSerialization} from "../util/serialization";
import {loadDefaultParts} from "../skeletons/default_parts";
import {loadMods} from "../player/mods";
import {loadDimensionDescriptions} from "../player/dimensions";

export let loaded = false;

/**
 * Load all necessary module components and assets
 * This function should be called AFTER all extensions of the system is done
 * and BEFORE calling any drawing methods
 */
export function load() {
    return new Promise((resolve) => {
        loadDimensionDescriptions();
        loadMods();
        loadDefaultParts();
        loadSerialization();
        loadPatterns();
        loaded = true;
        resolve();
    });
}

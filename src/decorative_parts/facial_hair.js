import {DecorativePart} from "./decorative_part";
import {Layer} from "../util/canvas";
import {none, adjustColor} from "drawpoint";

class FacialHair extends DecorativePart {
    constructor(...data) {
        super({
            loc       : "+head",
            layer     : Layer.BELOW_HAIR,
            aboveParts: ["parts head"],
        }, ...data);
    }

    stroke() {
        return none;
    }

    clipFill() {

    }

    fill(ignore, ex) {
        console.log(this);
        return adjustColor(ex.hairFill,
            {
                l: -5,
                s: -5
            });
    }
}


export class Mustache extends FacialHair {
    constructor(...data) {
        super(...data);
    }

    stroke() {
        return none;
    }

    clipFill() {

    }

    fill(ignore, ex) {
        console.log(this);
        return adjustColor(ex.hairFill,
            {
                l: -5,
                s: -5
            });
    }
}
